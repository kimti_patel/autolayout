//
//  ViewController.swift
//  Autolayout
//
//  Created by kim on 1-03-16.
//  Copyright © 2016 SkyeApps. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet var textField1: UITextField!
    
    @IBOutlet var textField2: UITextField!
    
    var keyBoardVisible : Bool = false
    
    override func viewDidLoad()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
        
    }

    func keyboardWillShow(sender: NSNotification)
    {
        print("show keyboard")
        if(keyBoardVisible == false && UIDevice.currentDevice().userInterfaceIdiom == .Phone)
        {
            self.view.frame.origin.y -= 150
           keyBoardVisible = true
        }
    }

    func keyboardWillHide(sender: NSNotification)
    {
        print("hide1 keyboard")

        if(keyBoardVisible == true && UIDevice.currentDevice().userInterfaceIdiom == .Phone)
        {
            self.view.frame.origin.y += 150
            keyBoardVisible = false
        }
    }
    
    
    
//    func textFieldShouldReturn(textField: UITextField) -> Bool
//    {
//        print("hide 2 keyboard")
//
//        textField.resignFirstResponder()
//        
//        keyBoardVisible = false
//
//        return true
//    }
    

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        print("hide3 keyboard")

        self.textField1.resignFirstResponder()
        self.textField2.resignFirstResponder()
        
        keyBoardVisible = false

    }
    
    @IBAction func saveButton (sender: UIButton )
    {
        self.textField1.text = ""
        self.textField2.text = ""
    }
}